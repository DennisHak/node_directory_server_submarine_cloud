package nl.han.asd.submarine.nodedirectoryserver.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExceptionHandlerControllerTest {

    private ExceptionHandlerController sut;

    @BeforeEach
    void setup() {
        sut = new ExceptionHandlerController();
    }

    @Test
    void handleCouldNotFindNodeByHostnameAndPortException() {
        // Act
        var actualResult = sut.handleCouldNotFindNodeByHostnameAndPortException();

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, actualResult.getStatusCode());
        assertEquals("Could not find node by hostname and port", actualResult.getBody().toString());
    }

    @Test
    void handleHostnameAndPortAlreadyExistException() {
        // Act
        var actualResult = sut.handleHostnameAndPortAlreadyExistException();

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, actualResult.getStatusCode());
        assertEquals("There is already an existing node with that hostname and port", actualResult.getBody().toString());
    }
}
