package nl.han.asd.submarine.nodedirectoryserver.controllers;

import nl.han.asd.submarine.nodedirectoryserver.models.Node;
import nl.han.asd.submarine.nodedirectoryserver.models.NodeDTO;
import nl.han.asd.submarine.nodedirectoryserver.models.NodesDTO;
import nl.han.asd.submarine.nodedirectoryserver.services.NodeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class NodeControllerTest {

    @InjectMocks
    private NodeController sut;

    @Mock
    private NodeService nodeServiceMock;

    @Mock
    private ModelMapper modelMapper;

    @Test
    void insertNode_callsServiceWithInsertedDTO() {
        // Init
        NodeDTO nodeDTO = new NodeDTO();
        Node node = new Node();
        when(modelMapper.map(nodeDTO, Node.class)).thenReturn(node);

        // Call
        sut.insertNode(nodeDTO);

        // Assert
        verify(nodeServiceMock).insertNode(node);
    }

    @Test
    void updateNode_callsServiceWithInsertedDTO() {
        // Init
        NodeDTO nodeDTO = new NodeDTO();
        Node node = new Node();
        when(modelMapper.map(nodeDTO, Node.class)).thenReturn(node);

        // Call
        sut.updateNode(nodeDTO);

        // Assert
        verify(nodeServiceMock).updateNode(node);
    }

    @Test
    void getActiveNodes_withNodesDTOreturned_hasEqualKey() {
        // Init
        NodeDTO nodeDTO = new NodeDTO();
        nodeDTO.setKey("SOMEKEY");
        NodesDTO nodesDTO = new NodesDTO();
        nodesDTO.getNodes().add(nodeDTO);

        when(nodeServiceMock.getActiveNodes()).thenReturn(nodesDTO);

        // Call
        NodesDTO result = sut.getActiveNodes();

        // Assert
        assertEquals(nodeDTO.getKey(), result.getNodes().get(0).getKey());
    }
}
