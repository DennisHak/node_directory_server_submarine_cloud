package nl.han.asd.submarine.nodedirectoryserver.integrationTests;

import nl.han.asd.submarine.nodedirectoryserver.models.Node;
import nl.han.asd.submarine.nodedirectoryserver.repositories.NodeRepository;
import nl.han.asd.submarine.nodedirectoryserver.services.NodeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Tag("integration-test")
@SpringBootTest
@AutoConfigureMockMvc
class SaveNodeIT {

    @Autowired
    private NodeRepository repository;

    @Autowired
    private NodeService nodeService;

    @BeforeEach
    void beforeEach() {
        repository.deleteAll();
    }

    @Test
    void checkIfNodeIsSavedToDb() {
        // Init
        Node node = new Node();
        node.setKey("key");
        node.setPort(1000);
        node.setHostname("hostname");
        repository.save(node);

        // Call
        List<Node> result = repository.findAll();

        // Assert
        assertAll(
                () -> assertEquals(1, result.size()),
                () -> assertEquals("key", result.get(0).getKey()),
                () -> assertEquals(1000, result.get(0).getPort()),
                () -> assertEquals("hostname", result.get(0).getHostname()));
    }

    @Test
    void checkIfNodeExistsInDb() {
        // Init
        Node node = new Node();
        node.setKey("key");
        node.setPort(1000);
        node.setHostname("hostname");
        repository.save(node);

        // Call
        boolean result = repository.existsNodeByHostnameAndPort("hostname", 1000);

        // Assert
        assertTrue(result);
    }

    @Test
    void checkIfNodeNotExistsInDb() {
        // Init
        Node node = new Node();
        node.setKey("key");
        node.setPort(1000);
        node.setHostname("hostname");
        repository.save(node);

        // Call
        boolean result = repository.existsNodeByHostnameAndPort("hans", 1000);

        // Assert
        assertFalse(result);
    }

    @Test
    void checkIfFindAllFindsAllNodes() {
        // Init
        Node node = new Node();
        node.setKey("key");
        node.setPort(1000);
        node.setHostname("hostname");
        repository.save(node);

        Node node1 = new Node();
        node1.setKey("KEY");
        node1.setPort(10000);
        node1.setHostname("HOSTNAME");
        repository.save(node1);

        // Call
        List<Node> result = repository.findAll();

        // Assert
        assertAll(
                () -> assertEquals(2, result.size()),
                () -> assertEquals(node.getHostname(), result.get(0).getHostname()),
                () -> assertEquals(node.getKey(), result.get(0).getKey()),
                () -> assertEquals(node.getPort(), result.get(0).getPort()),
                () -> assertEquals(node1.getHostname(), result.get(1).getHostname()),
                () -> assertEquals(node1.getKey(), result.get(1).getKey()),
                () -> assertEquals(node1.getPort(), result.get(1).getPort())
        );
    }

    @Test
    void updateNodeInDb() {
        // Init
        Node node = new Node();
        node.setKey("key");
        node.setPort(1000);
        node.setHostname("hostname");
        repository.save(node);

        node.setKey("UPDATEDKEY");

        // Call
        nodeService.updateNode(node);
        List<Node> result = repository.findAll();

        // Assert
        assertAll(
                () -> assertEquals(1, result.size()),
                () -> assertEquals(node.getHostname(), result.get(0).getHostname()),
                () -> assertEquals(node.getKey(), result.get(0).getKey()),
                () -> assertEquals(node.getPort(), result.get(0).getPort())
        );
    }
}
