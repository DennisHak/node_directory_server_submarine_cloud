package nl.han.asd.submarine.nodedirectoryserver.util;

import nl.han.asd.submarine.nodedirectoryserver.exceptions.FieldCheckerException;
import nl.han.asd.submarine.nodedirectoryserver.models.Node;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FieldCheckerTest {

    Node nodeMock;

    @BeforeEach
    void setup() {
        nodeMock = mock(Node.class);
        when(nodeMock.getHostname()).thenReturn("TEST");
        when(nodeMock.getKey()).thenReturn("TEST");
    }

    @Test
    void allFieldsOfClassAreSet() {
        // Call & Assert
        assertTrue(FieldChecker.areAllFieldsSet(new String[]{"hostname", "key"}, nodeMock));
    }

    @Test
    void returnsFalseIfValueEmpty() {
        // Call & Assert
        assertFalse(FieldChecker.areAllFieldsSet(new String[]{"hostname", "key", "id"}, nodeMock));
    }

    @Test
    void returnsExceptionIfAGetterIsNotDefined() {
        try {
            // Call
            FieldChecker.areAllFieldsSet(new String[]{"notExistingMethod"}, nodeMock);

            //Expect error to be thrown, if not, fail test
            fail("Expected an FieldCheckerException to be thrown");
        } catch (FieldCheckerException e) {
            // Assert
            assertEquals("Something went wrong. With cause: null Contact the system admin", e.getMessage());
        }
    }
}


