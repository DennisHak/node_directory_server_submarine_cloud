package nl.han.asd.submarine.nodedirectoryserver.services.impl;

import nl.han.asd.submarine.nodedirectoryserver.exceptions.CouldNotFindNodeByHostnameAndPortException;
import nl.han.asd.submarine.nodedirectoryserver.exceptions.HostnameAndPortAlreadyExistException;
import nl.han.asd.submarine.nodedirectoryserver.models.Node;
import nl.han.asd.submarine.nodedirectoryserver.models.NodeDTO;
import nl.han.asd.submarine.nodedirectoryserver.models.NodesDTO;
import nl.han.asd.submarine.nodedirectoryserver.repositories.NodeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class NodeServiceImplTest {

    @InjectMocks
    NodeServiceImpl sut;

    @Mock
    NodeRepository nodeRepositoryMock;

    @Mock
    ModelMapper modelMapperMock;

    private Node node;

    @BeforeEach
    void setUp() {
        node = new Node();
        node.setHostname("HOSTNAME");
        node.setPort(2097);
        node.setKey("JUST_A_KEY");
    }

    @Test
    void insertNode_withInvalidArguments_throwsIllegalArgumentsException() {
        Node testNode = new Node();
        try {
            // Call
            sut.insertNode(testNode);

            //Expect error to be thrown, if not, fail test
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            // Assert
            assertEquals("Not all fields are filled in. Expected:\n" +
                    "{\n" +
                    "\t\"hostname\": (String),\n" +
                    "\t\"port\": (String),\n" +
                    "\t\"key\": (String)\n" +
                    "}", e.getMessage());
        }
    }

    @Test
    void insertNode_withValidArguments_throwsHostnameAndPortAlreadyExistException() {
        // Init
        when(nodeRepositoryMock.existsNodeByHostnameAndPort(node.getHostname(), node.getPort())).thenReturn(true);

        try {
            // Call
            sut.insertNode(node);

            //Expect error to be thrown, if not, fail test
            fail("Expected an HostnameAndPortAlreadyExistException to be thrown");
        } catch (HostnameAndPortAlreadyExistException e) {
            // Assert
            // Null because the error is only used to determine http-response
            assertNull(e.getMessage());
        }
    }

    @Test
    void insertNode_withValidArguments_callsSaveToCollection() {
        // Init
        when(nodeRepositoryMock.existsNodeByHostnameAndPort(node.getHostname(), node.getPort())).thenReturn(false);

        // Call
        sut.insertNode(node);

        // Assert
        verify(nodeRepositoryMock, times(1)).save(node);
    }

    @Test
    void getActiveNodes_withZeroNodesFound_returnsEmptyNodesDTO() {
        // Init
        when(nodeRepositoryMock.findAll()).thenReturn(new ArrayList<>());

        // Call
        NodesDTO result = sut.getActiveNodes();

        // Assert
        assertEquals(0, result.getNodes().size());
    }

    @Test
    void getActiveNodes_withNodesFound_returnsNodesDTOwithNodes() {
        // Init
        List<Node> nodes = new ArrayList<>();
        nodes.add(node);
        nodes.add(node);
        NodeDTO nodeDTO = new NodeDTO();
        nodeDTO.setKey("JUST_A_KEY");

        when(nodeRepositoryMock.findAll()).thenReturn(nodes);
        when(modelMapperMock.map(node, NodeDTO.class)).thenReturn(nodeDTO);

        // Call
        NodesDTO result = sut.getActiveNodes();

        // Assert
        assertAll(
                () -> assertEquals(2, result.getNodes().size()),
                () -> assertEquals("JUST_A_KEY", result.getNodes().get(0).getKey()),
                () -> verify(modelMapperMock, times(2)).map(node, NodeDTO.class)
        );
    }

    @Test
    void updateNode_withNoExistingHostNameAndPort_throwsCouldNotFindNodeByHostnameAndPortException() {
        // Init
        when(nodeRepositoryMock.existsNodeByHostnameAndPort(node.getHostname(), node.getPort())).thenReturn(false);

        try {
            // Call
            sut.updateNode(node);

            //Expect error to be thrown, if not, fail test
            fail("Expected an CouldNotFindNodeByHostnameAndPortException to be thrown");
        } catch (CouldNotFindNodeByHostnameAndPortException e) {
            // Assert
            // Null because the error is only used to determine http-response
            assertNull(e.getMessage());
        }
    }

    @Test
    void updateNode_withExistingHostNameAndPort_callsFunctionToSave() {
        // Init
        when(nodeRepositoryMock.existsNodeByHostnameAndPort(node.getHostname(), node.getPort())).thenReturn(true);
        when(nodeRepositoryMock.findNodeByHostnameAndPort(node.getHostname(), node.getPort())).thenReturn(node);

        // Call
        sut.updateNode(node);

        // Assert
        verify(nodeRepositoryMock, times(1)).save(node);
    }
}
