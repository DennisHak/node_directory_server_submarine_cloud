package nl.han.asd.submarine.nodedirectoryserver.integrationTests;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.han.asd.submarine.nodedirectoryserver.models.Destination;
import nl.han.asd.submarine.nodedirectoryserver.models.Node;
import nl.han.asd.submarine.nodedirectoryserver.models.NodeDTO;
import nl.han.asd.submarine.nodedirectoryserver.models.NodesDTO;
import nl.han.asd.submarine.nodedirectoryserver.repositories.NodeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@Tag("integration-test")
@SpringBootTest
@AutoConfigureMockMvc
class NodeControllerIT {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    NodeRepository nodeRepository;

    @MockBean
    ModelMapper modelMapper;

    private Node node;

    @BeforeEach
    void setup() {
        node = new Node();
        node.setKey("KEY");
        node.setPort(104);
        node.setHostname("HOSTNAME");
    }

    @Test
    void insertNode_withExistingNode_returnsBadRequest() throws Exception {
        // Init
        Destination destination = new Destination("HOSTNAME", 104);
        NodeDTO nodeDTO = new NodeDTO(destination, "KEY");
        when(modelMapper.map(any(), any())).thenReturn(node);
        when(nodeRepository.existsNodeByHostnameAndPort("HOSTNAME", 104)).thenReturn(true);

        // Call
        MvcResult result = mockMvc.perform(post("/node/create", nodeDTO)).andReturn();

        // Assert
        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void insertNode_withNoExistingNode_returnsCreatedStatus() throws Exception {
        // Init
        Destination destination = new Destination("HOSTNAME", 104);
        NodeDTO nodeDTO = new NodeDTO(destination, "KEY");
        when(modelMapper.map(any(), any())).thenReturn(node);
        when(nodeRepository.existsNodeByHostnameAndPort("HOSTNAME", 104)).thenReturn(false);

        // Call
        MvcResult result = mockMvc.perform(post("/node/create", nodeDTO)).andReturn();

        // Assert
        assertAll(
                () -> assertEquals(201, result.getResponse().getStatus()),
                () -> verify(nodeRepository, times(1)).save(node)
        );
    }

    @Test
    void getActiveNodes_withNoActiveNodes_returnsEmptyDTO() throws Exception {
        // Init
        List<Node> nodes = new ArrayList<>();
        when(nodeRepository.findAll()).thenReturn(nodes);

        // Call
        MvcResult result = mockMvc.perform(get("/node").contentType(MediaType.APPLICATION_JSON)).andReturn();
        NodesDTO resultObject = new ObjectMapper().readValue(result.getResponse().getContentAsString(), NodesDTO.class);

        // Assert
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(0, resultObject.getNodes().size())
        );
    }

    @Test
    void getActiveNodes_withActiveNodes_returnsDTOWithValues() throws Exception {
        // Init
        List<Node> nodes = new ArrayList<>();
        nodes.add(node);
        nodes.add(node);
        when(nodeRepository.findAll()).thenReturn(nodes);

        // Call
        MvcResult result = mockMvc.perform(get("/node").contentType(MediaType.APPLICATION_JSON)).andReturn();
        NodesDTO resultObject = new ObjectMapper().readValue(result.getResponse().getContentAsString(), NodesDTO.class);

        // Assert
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(2, resultObject.getNodes().size())
        );
    }

    @Test
    void updateNode_withNotExistingNode_returnsBadRequest() throws Exception {
        // Init
        Destination destination = new Destination("HOSTNAME", 104);
        NodeDTO nodeDTO = new NodeDTO(destination, "KEY");
        when(modelMapper.map(any(), any())).thenReturn(node);
        when(nodeRepository.existsNodeByHostnameAndPort("HOSTNAME", 104)).thenReturn(false);

        // Call
        MvcResult result = mockMvc.perform(put("/node/update", nodeDTO)).andReturn();

        // Assert
        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void updateNode_withExistingNode_returnsOk() throws Exception {
        // Init
        Destination destination = new Destination("HOSTNAME", 104);
        NodeDTO nodeDTO = new NodeDTO(destination, "KEY");
        when(modelMapper.map(any(), any())).thenReturn(node);
        when(nodeRepository.existsNodeByHostnameAndPort("HOSTNAME", 104)).thenReturn(true);
        when(nodeRepository.findNodeByHostnameAndPort("HOSTNAME", 104)).thenReturn(node);

        // Call
        MvcResult result = mockMvc.perform(put("/node/update", nodeDTO)).andReturn();

        // Assert
        assertEquals(200, result.getResponse().getStatus());
    }
}
