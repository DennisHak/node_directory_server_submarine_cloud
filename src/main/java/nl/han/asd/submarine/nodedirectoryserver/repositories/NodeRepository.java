package nl.han.asd.submarine.nodedirectoryserver.repositories;

import nl.han.asd.submarine.nodedirectoryserver.models.Node;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NodeRepository extends MongoRepository<Node, String> {
    boolean existsNodeByHostnameAndPort(String hostname, int port);

    Node findNodeByHostnameAndPort(String hostname, int port);
}
