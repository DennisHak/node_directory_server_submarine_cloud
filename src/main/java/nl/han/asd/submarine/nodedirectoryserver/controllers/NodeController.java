package nl.han.asd.submarine.nodedirectoryserver.controllers;

import nl.han.asd.submarine.nodedirectoryserver.models.Node;
import nl.han.asd.submarine.nodedirectoryserver.models.NodeDTO;
import nl.han.asd.submarine.nodedirectoryserver.models.NodesDTO;
import nl.han.asd.submarine.nodedirectoryserver.services.NodeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/node")
public class NodeController {

    @Autowired
    private NodeService nodeService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public void insertNode(NodeDTO nodeDTO) {
        nodeService.insertNode(modelMapper.map(nodeDTO, Node.class));
    }

    @PutMapping("/update")
    @ResponseStatus(HttpStatus.OK)
    public void updateNode(NodeDTO nodeDTO) {
        nodeService.updateNode(modelMapper.map(nodeDTO, Node.class));
    }

    @GetMapping(produces = "application/json")
    public NodesDTO getActiveNodes() {
        return nodeService.getActiveNodes();
    }
}
