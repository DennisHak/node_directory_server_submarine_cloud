package nl.han.asd.submarine.nodedirectoryserver.models;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class NodeDTO {

    private Destination destination;
    private String key;

    public NodeDTO() {
    }

    public NodeDTO(final Destination destination, final String key) {
        this.destination = destination;
        this.key = key;
    }
}
