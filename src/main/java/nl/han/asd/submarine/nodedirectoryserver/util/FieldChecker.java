package nl.han.asd.submarine.nodedirectoryserver.util;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.base.CaseFormat;
import nl.han.asd.submarine.nodedirectoryserver.exceptions.FieldCheckerException;

public class FieldChecker {

    private static final Logger LOG = Logger.getLogger(FieldChecker.class.getName());

    private FieldChecker() {
    }

    private static <T> boolean checkFieldNotNull(String field, T object) {
        try {
            return object.getClass().getMethod("get" + CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, field)).invoke(object) != null;
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            LOG.log(Level.SEVERE, String.format("A gettermethod was not found. Method in question is `get%s`\n" +
                    "Stacktrace:\n" +
                    "%s", CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, field), Arrays.toString(e.getStackTrace())));
            throw new FieldCheckerException("Something went wrong. With cause: " + e.getCause() + " Contact the system admin");
        }
    }

    public static <T> boolean areAllFieldsSet(String[] fields, T object) {
        return Arrays.stream(fields).allMatch(field -> FieldChecker.checkFieldNotNull(field, object));
    }
}
