package nl.han.asd.submarine.nodedirectoryserver.services;

import nl.han.asd.submarine.nodedirectoryserver.models.Node;
import nl.han.asd.submarine.nodedirectoryserver.models.NodesDTO;

public interface NodeService {
    void insertNode(Node node);

    NodesDTO getActiveNodes();

    void updateNode(Node node);
}
