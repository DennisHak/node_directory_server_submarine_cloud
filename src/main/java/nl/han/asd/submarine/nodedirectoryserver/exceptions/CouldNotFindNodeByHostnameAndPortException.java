package nl.han.asd.submarine.nodedirectoryserver.exceptions;

public class CouldNotFindNodeByHostnameAndPortException extends RuntimeException {

    public CouldNotFindNodeByHostnameAndPortException() {
        super();
    }
}
