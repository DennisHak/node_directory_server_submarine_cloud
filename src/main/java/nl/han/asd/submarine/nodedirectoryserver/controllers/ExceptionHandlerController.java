package nl.han.asd.submarine.nodedirectoryserver.controllers;

import nl.han.asd.submarine.nodedirectoryserver.exceptions.CouldNotFindNodeByHostnameAndPortException;
import nl.han.asd.submarine.nodedirectoryserver.exceptions.HostnameAndPortAlreadyExistException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController {
    @ExceptionHandler(value = CouldNotFindNodeByHostnameAndPortException.class)
    public ResponseEntity<Object> handleCouldNotFindNodeByHostnameAndPortException() {
        return new ResponseEntity<>("Could not find node by hostname and port", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = HostnameAndPortAlreadyExistException.class)
    public ResponseEntity<Object> handleHostnameAndPortAlreadyExistException() {
        return new ResponseEntity<>("There is already an existing node with that hostname and port", HttpStatus.BAD_REQUEST);
    }
}
