package nl.han.asd.submarine.nodedirectoryserver.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Destination {
    private String hostname;
    private int port;

    public Destination(final String hostname, final int port) {
        this.hostname = hostname;
        this.port = port;
    }
}
