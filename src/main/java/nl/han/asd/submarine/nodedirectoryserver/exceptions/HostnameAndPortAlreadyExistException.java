package nl.han.asd.submarine.nodedirectoryserver.exceptions;

public class HostnameAndPortAlreadyExistException extends RuntimeException {
    public HostnameAndPortAlreadyExistException() {
        super();
    }
}
