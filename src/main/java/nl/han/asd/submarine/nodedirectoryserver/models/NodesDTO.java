package nl.han.asd.submarine.nodedirectoryserver.models;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class NodesDTO {
    private List<NodeDTO> nodes;

    public NodesDTO(List<NodeDTO> nodes) {
        this.nodes = nodes;
    }

    public NodesDTO() {
        nodes = new ArrayList<>();
    }
}
