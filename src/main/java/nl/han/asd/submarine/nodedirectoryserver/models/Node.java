package nl.han.asd.submarine.nodedirectoryserver.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "node")
public class Node {
    @Id
    private String id;

    private String hostname;
    private int port;
    private String key;

    public Node() {
    }

    public Node(String hostname, int port, String key) {
        this.hostname = hostname;
        this.port = port;
        this.key = key;
    }
}
