package nl.han.asd.submarine.nodedirectoryserver.services.impl;

import nl.han.asd.submarine.nodedirectoryserver.exceptions.CouldNotFindNodeByHostnameAndPortException;
import nl.han.asd.submarine.nodedirectoryserver.exceptions.HostnameAndPortAlreadyExistException;
import nl.han.asd.submarine.nodedirectoryserver.models.Node;
import nl.han.asd.submarine.nodedirectoryserver.models.NodeDTO;
import nl.han.asd.submarine.nodedirectoryserver.models.NodesDTO;
import nl.han.asd.submarine.nodedirectoryserver.repositories.NodeRepository;
import nl.han.asd.submarine.nodedirectoryserver.services.NodeService;
import nl.han.asd.submarine.nodedirectoryserver.util.FieldChecker;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NodeServiceImpl implements NodeService {

    @Autowired
    private NodeRepository collection;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void insertNode(Node node) {
        if (!FieldChecker.areAllFieldsSet(new String[]{"hostname", "port", "key"}, node)) {
            throw new IllegalArgumentException("Not all fields are filled in. Expected:\n" +
                    "{\n" +
                    "\t\"hostname\": (String),\n" +
                    "\t\"port\": (String),\n" +
                    "\t\"key\": (String)\n" +
                    "}");
        } else if (collection.existsNodeByHostnameAndPort(node.getHostname(), node.getPort())) {
            throw new HostnameAndPortAlreadyExistException();
        }
        collection.save(node);
    }

    @Override
    public NodesDTO getActiveNodes() {
        List<Node> nodes = collection.findAll();
        NodesDTO nodesDTO = new NodesDTO();

        nodes.stream()
                .map(node -> modelMapper.map(node, NodeDTO.class))
                .forEach(nodesDTO.getNodes()::add);

        return nodesDTO;
    }

    @Override
    public void updateNode(Node node) {
        if (collection.existsNodeByHostnameAndPort(node.getHostname(), node.getPort())) {
            Node document = collection.findNodeByHostnameAndPort(node.getHostname(), node.getPort());
            document.setKey(node.getKey());
            collection.save(document);
        } else {
            throw new CouldNotFindNodeByHostnameAndPortException();
        }
    }
}
