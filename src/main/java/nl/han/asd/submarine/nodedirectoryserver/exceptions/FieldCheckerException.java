package nl.han.asd.submarine.nodedirectoryserver.exceptions;

public class FieldCheckerException extends RuntimeException {

    public FieldCheckerException(String message) {
        super(message);
    }
}
