# Node Directory Server: Java Spring
This is the repository for the node directory server.

## Prerequisites
- An installation of MongoDB is mandatory for this server.

## Useful links
Cloud repo: https://bitbucket.org/DennisHak/node_directory_server_submarine_cloud/src/  
Sonar: https://sonarcloud.io/project/dashboard?branch=development&id=Dennis-Hakvoort_node_directory_server_submarine_cloud

## Endpoints

### Create node
```
url:            /node/create
method:         POST
status code:    201 (Created)
```

Request body need to be conform the NodeDTO model. Example in JSON:
```
{
    "destination": {
        "hostname": "xxx.xxx.com"
        "port": 25010},
    "key": "9H+5ObOd2IO/KQcGTTnKR5h3F1DrWFjXt0oud+zlL0Q="
}
```
Of note here is that the key should be a base64 encoded string representing the bytes of the secret key

Common exceptions:
```
status 400:     Bad Request
message:        "Fill in all fields"
```
### Update node
```
url:            /node/update
method:         POST
status code:    200 (OK)
```

Request body need to be conform the NodeDTO model. Example in JSON:
```
{
    "destination": {
        "hostname": "xxx.xxx.com"
        "port": 25010},
    "key": "9H+5ObOd2IO/KQcGTTnKR5h3F1DrWFjXt0oud+zlL0Q="
}
```
Of note here is that the key should be a base64 encoded string representing the bytes of the secret key

Common exceptions:
```
status 400:     Bad Request
message:        "Fill in all fields"
```
### Get all available nodes
```
url:            /node
method:         GET
status code:    200 (OK)
```

Response body should be conform NodesDTO, which is a list of NodeDTO. For example:
```
{
    [{"destination": {
        "hostname": "xxx.xxx.com"
        "port": 25010},
    "key": "9H+5ObOd2IO/KQcGTTnKR5h3F1DrWFjXt0oud+zlL0Q="},
    "destination": {
        "hostname": "yyy.yyy.com"
        "port": 25010},
    "key": "9H+5ObOd2IO/KQcGTTnKR5h3F1DrWFjXt0oud+zlL0Q="}]
}
```

Common exceptions:
```
status 400:     Bad Request
```
